#include "fetchmetadata.h"

// Qt includes
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>

// QJSON includes
#include <qjson/parser.h>

// KDE includes
#include <KUrl>
#include <KDebug>

FetchMetaData::FetchMetaData(const QString identifier, QObject *parent) :
    QObject(parent),
    m_identifier(identifier)
{
}

void FetchMetaData::fetch()
{
    const static QLatin1String queryUrl("http://api.movies.io/movies/search?q=");
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkRequest request(KUrl(queryUrl + m_identifier));
    request.setRawHeader(QByteArray("Host"), QByteArray("api.movies.io"));
    request.setRawHeader(QByteArray("X-Target-URI"), QByteArray("http://api.movies.io"));
    request.setRawHeader(QByteArray("Connection"), QByteArray("Keep-Alive"));
    kDebug() << "Sending request to " << KUrl(queryUrl + m_identifier);
    m_reply = manager->get(request);

    connect(m_reply, SIGNAL(finished()),
                          SLOT(replyFinished()));
    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
                            SLOT(error(QNetworkReply::NetworkError)));
}

void FetchMetaData::replyFinished()
{
    kDebug() << m_reply->readAll();
    QJson::Parser parser;
    bool ok = false;
    QVariantMap map = parser.parse(m_reply->readAll(), &ok).toMap();
    kDebug() << map;
    if (map.isEmpty()) {
        kDebug() << "Empty map :|";
    }
    if (!ok) {
        kDebug() << "Parsing failed :(";
    } else {
        kDebug() << "Parsing sucessfull!";
    }

    kDebug() << map;

    m_reply->deleteLater();
}

void FetchMetaData::error(QNetworkReply::NetworkError error)
{
    kDebug() << "Error!!" << error;
}
