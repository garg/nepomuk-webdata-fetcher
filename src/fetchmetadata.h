#ifndef FETCHMETADATA_H
#define FETCHMETADATA_H

#include <QObject>
#include <QNetworkReply>

class QString;

class FetchMetaData : public QObject
{
    Q_OBJECT
public:
    explicit FetchMetaData(const QString, QObject *parent = 0);
    void fetch();
    
//signals:
    
//public slots:

private:
    QString m_identifier;
    QNetworkReply *m_reply;

private slots:
    void replyFinished();
    void error(QNetworkReply::NetworkError error);
    
};

#endif // FETCHMETADATA_H
