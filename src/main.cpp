// Qt includes
#include <QtCore/QCoreApplication>

// KDE includes
#include <KAboutData>
#include <KCmdLineArgs>
#include <KComponentData>
#include <KLocale>

#include <KDebug>

// Local includes
#include "fetchmetadata.h"

int main(int argc, char *argv[])
{    
    KAboutData aboutData("nepomukwebextractor", 0, ki18n("NepomukWebExtractor"),
                         0,
                 ki18n("NepomukWebExtractor fetches data from web services for a richer KDE experience"),
             KAboutData::License_LGPL_V2,
             ki18n("(C) 2012, Rohan Garg"));
    aboutData.addAuthor(ki18n("Rohan Garg"), ki18n("Developer"), "rohangarg@kubuntu.org");

    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions options;
    options.add("+[url]", ki18n("The URL of the file to be indexed"));
    KCmdLineArgs::addCmdLineOptions(options);
    const KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    if( args->count() == 0 ) {
        QTextStream err( stderr );
        err << "Must input url of the file to be indexed";

        return 1;
    }



    QCoreApplication a(argc, argv);

    FetchMetaData *request = new FetchMetaData(args->arg(0));
    request->fetch();
    return a.exec();
}

